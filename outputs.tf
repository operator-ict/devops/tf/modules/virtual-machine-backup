output "resource_group_name" {
  value = azurerm_resource_group.rg.name
}

output "backup_policy" {
  value = length(azurerm_backup_policy_vm.virtual_machine) > 0 ? zipmap(
                                                                values(azurerm_backup_policy_vm.virtual_machine)[*].name,
                                                                values(azurerm_backup_policy_vm.virtual_machine)[*].id) : {}
}

output "recovery_vault_name" {
  value = length(azurerm_recovery_services_vault.vault) >0 ? azurerm_recovery_services_vault.vault[0].name : null
}
