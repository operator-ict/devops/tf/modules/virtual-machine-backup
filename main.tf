terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">= 4.2.0, < 5.0.0"
    }
  }
}

resource "azurerm_resource_group" "rg" {
  name     = "rg-vm-backup-${var.name}"
  location = var.location
  tags     = var.tags
}

resource "azurerm_recovery_services_vault" "vault" {
  count               = var.backup_vm_vault != null ? 1:0
  name                = var.backup_vm_vault
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  sku                 = "RS0"

  soft_delete_enabled = true
}

resource "azurerm_backup_policy_vm" "virtual_machine" {
  for_each                       = var.backup_vm_policies
  name                           = each.key
  resource_group_name            = azurerm_resource_group.rg.name
  recovery_vault_name            = azurerm_recovery_services_vault.vault[0].name
  instant_restore_retention_days = 2
  timezone = "UTC"

  backup {
    frequency = each.value.frequency
    time      = each.value.time
  }

  retention_daily {
    count = each.value.retention
  }

}

