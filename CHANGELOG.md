# Unreleased
## Added
## Changed
## Fixed

# [0.2.0] - 2024-09-18
## Changed
- use azurerm >=4.2.0

# [0.1.0] - 2024-03-20
## Release notes
Create backup vault and policies
## Added
- init
