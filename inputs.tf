variable "location" {
  type = string
}

variable "name" {
  type = string
}

variable "tags" {
  type = map(string)
}

variable "backup_vm_vault" {
  type = string
  default = null
}

variable "backup_vm_policies" {
  type = map(object({
    frequency = string
    time = string
    retention = number
  }))
  default = {}
}
